#ifndef QWKPACKET_H
#define QWKPACKET_H

#include <QString>
#include <QList>
#include "messagegroup.h"
class QwkPacket
{
public:
    QwkPacket();
    ~QwkPacket();
    bool unpack_zip(QString filename);
    bool unpack(QString filename, QString unarchive_cmd);
    bool load_qwk(bool qwkesupport);
    void update_read(uint32_t offset, bool read);

    QList<messagegroup *> *groups;
    QString bbsname;
    QString bbslocation;
    QString sysopname;
    QString username;
    QString bbsqwkid;
    QString packetdate;
    QString getLastError();

private:
    QString lastError;
};

#endif // QWKPACKET_H
