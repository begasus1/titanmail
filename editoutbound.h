#ifndef EDITOUTBOUND_H
#define EDITOUTBOUND_H

#include <QDialog>

class MainWindow;
struct ob_msg;
class Outbound;

namespace Ui {
class EditOutbound;
}

class EditOutbound : public QDialog
{
    Q_OBJECT

public:
    explicit EditOutbound(QWidget *parent = nullptr, MainWindow *mw = nullptr, struct ob_msg *obmsg = nullptr);
    ~EditOutbound();

private slots:
    void on_buttonBox_accepted();

private:
    Ui::EditOutbound *ui;

    struct ob_msg *msg;
    MainWindow *mw;
    Outbound *ob;
};

#endif // EDITOUTBOUND_H
