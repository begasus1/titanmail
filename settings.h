#ifndef SETTINGS_H
#define SETTINGS_H

#include <QDialog>
#include "mainwindow.h"
namespace Ui {
class Settings;
}

class Settings : public QDialog
{
    Q_OBJECT

public:
    explicit Settings(QWidget *parent = nullptr);
    ~Settings();

private slots:
    void on_buttonBox_accepted();

    void on_taglineToolBtn_clicked();

private:
    Ui::Settings *ui;
    MainWindow *parent;
};

#endif // SETTINGS_H
