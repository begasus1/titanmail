#ifndef NEWMSG_H
#define NEWMSG_H

#include <QDialog>


class MainWindow;

namespace Ui {
class NewMsg;
}

class NewMsg : public QDialog
{
    Q_OBJECT

public:
    explicit NewMsg(QWidget *parent, QString subject, QString to, QString quote, QString bbstag, int area, QString username, int replyid, bool qwke);
    ~NewMsg();
    void addMessage(QString msgtag);

private slots:
    void on_pushButton_clicked();
    void on_pushButton_2_clicked();

private:
    MainWindow *mw;
    Ui::NewMsg *ui;
    QString qwkid;
    int areano;
    QString username;
    int replyid;
    bool qwkesupport;
};

#endif // NEWMSG_H
