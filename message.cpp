#include "message.h"

Message::Message(int no, QString subject, QString from, QString to, QDateTime date, QString body)
{
    msgno = no;
    this->subject = subject;
    this->from = from;
    this->to = to;
    this->date = date;
    this->body = body;
}
