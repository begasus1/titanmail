#include <QProcess>
#include <QTemporaryDir>
#include <QRegularExpression>
#include <QStandardPaths>
#include <quazip/JlCompress.h>
#include "message.h"
#include "qwkpacket.h"
#include "qwkheader.h"

static int safe_atoi(const char *str, int len) {
    int ret = 0;

    for (int i=0;i<len;i++) {
        if (str[i] < '0' || str[i] > '9') {
            break;
        }
        ret = ret * 10 + (str[i] - '0');
    }
    return ret;
}

static QString rstrip(const QString& str) {
  int n = str.size() - 1;
  for (; n >= 0; --n) {
    if (!str.at(n).isSpace()) {
      return str.left(n + 1);
    }
  }
  return "";
}

QString QwkPacket::getLastError()
{
    return lastError;
}

QwkPacket::QwkPacket()
{
    groups = new QList<messagegroup *>();
}

QwkPacket::~QwkPacket()
{
    delete groups;
}

bool QwkPacket::unpack_zip(QString filename) {
    QString pth  = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
    QDir dir(pth + "/_work");

    if (dir.exists()) {
        dir.removeRecursively();
    }

    QDir().mkpath(dir.path());


    JlCompress::extractDir(filename, dir.absolutePath());

    return true;
}

bool QwkPacket::unpack(QString filename, QString unarchive_cmd) {
    if (filename.right(4).toUpper() == ".QWK") {
        QProcess unarc;
        QString pth  = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);

        QDir dir(pth + "/_work");

        if (dir.exists()) {
            dir.removeRecursively();
        }

        QDir().mkpath(dir.path());

        QStringList list = unarchive_cmd.split(QRegExp("\\s+"));
        QString cmd = list.first();
        QStringList args;

        for (int z = 1; z < list.count();z++) {
            if (list.at(z) == "$DIRECTORY") {
                args << dir.path();
            } else if (list.at(z) == "$ARCHIVE") {
                args << filename;
            } else {
                args << list.at(z);
            }
        }


        if (unarc.execute(cmd, args) != 0) {
            lastError = "Failed to unzip QWK packet. ";
            return false;
        }
    }
    return true;
}

void QwkPacket::update_read(uint32_t offset, bool read) {
    FILE *fptr;
    char c;

    QString pth  = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);

    QDir dir(pth + "/_work");

    if (!dir.exists()) {
        return;
    }

    if (dir.exists("messages.dat")) {
        fptr = fopen(dir.filePath("messages.dat").toStdString().c_str(), "r+b");
    } else {
        fptr = fopen(dir.filePath("MESSAGES.DAT").toStdString().c_str(), "r+b");
    }
    if (!fptr) {
        lastError = "Failed to open messages.dat";
        return;
    }

    fseek(fptr, offset, SEEK_SET);

    if (read) {
        c = '-';
    } else {
        c = ' ';
    }

    fwrite(&c, 1, 1, fptr);
    fclose(fptr);
}

bool QwkPacket::load_qwk(bool qwkesupport) {
    FILE *fptr;
    struct QwkHeader qhdr;
    char buffer[256];


    QString pth  = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);

    QDir dir(pth + "/_work");

    if (!dir.exists()) {
        return false;
    }

    fptr = fopen(dir.filePath("control.dat").toStdString().c_str(), "r");
    if (!fptr) {
        fptr = fopen(dir.filePath("CONTROL.DAT").toStdString().c_str(), "r");
        if (!fptr) {
            lastError = "Failed to open control.dat";
            return false;
        }
    }


    fgets(buffer, sizeof buffer, fptr);
    bbsname = QString(buffer).trimmed();

    fgets(buffer, sizeof buffer, fptr);
    bbslocation = QString(buffer).trimmed();

    fgets(buffer, sizeof buffer, fptr); // phone number

    fgets(buffer, sizeof buffer, fptr);
    sysopname = QString(buffer).trimmed();
    fgets(buffer, sizeof buffer, fptr);
    bbsqwkid = QString(strchr(buffer, ',') + 1).trimmed();

    fgets(buffer, sizeof buffer, fptr);
    packetdate = QString(buffer).trimmed();

    fgets(buffer, sizeof buffer, fptr);
    username = QString(buffer).trimmed();
    fgets(buffer, sizeof buffer, fptr);
    fgets(buffer, sizeof buffer, fptr);
    fgets(buffer, sizeof buffer, fptr);
    fgets(buffer, sizeof buffer, fptr);
    int totareas = QString(buffer).trimmed().toInt();
    int count = 0;

    while (!feof(fptr) && count <= totareas) {
        fgets(buffer, sizeof buffer, fptr);
        int area = QString(buffer).trimmed().toInt();
        fgets(buffer, sizeof buffer, fptr);
        messagegroup *gr = new messagegroup(area);
        gr->groupname = QString::number(area) + " - " + QString(buffer).trimmed();
        groups->append(gr);
        count++;
    }

    fclose(fptr);

    fptr = fopen(dir.filePath("messages.dat").toStdString().c_str(), "rb");
    if (!fptr) {
        fptr = fopen(dir.filePath("MESSAGES.DAT").toStdString().c_str(), "rb");
        if (!fptr) {
            lastError = "Failed to open messages.dat";
            return false;
        }
    }

    fread(&qhdr, sizeof(struct QwkHeader), 1, fptr);


    while (!feof(fptr)) {
        uint32_t offset = ftell(fptr);

        if (fread(&qhdr, sizeof(struct QwkHeader), 1, fptr) != 1) {
            break;
        }

        int msg_recs = safe_atoi((const char *)qhdr.Msgrecs, 6);
        char *msg_body = (char *)malloc(((msg_recs -1) * 128) + 1);
        if (!msg_body) {
            lastError = "Failed to allocate memory.";
            fclose(fptr);
            return false;
        }
        memset(msg_body, 0, (msg_recs - 1) * 128 + 1);
        fread(msg_body, sizeof(struct QwkHeader), msg_recs - 1, fptr);
        for (size_t i = 0; i < strlen(msg_body); i++) {
            if (msg_body[i] == '\xe3') {
                msg_body[i] = '\n';
            }
        }

        QString Subject = QString((char *)qhdr.MsgSubj).left(25).trimmed();
        QString From = QString((char *)qhdr.MsgFrom).left(25).trimmed();
        QString To = QString((char *)qhdr.MsgTo).left(25).trimmed();
        QDate date;

        int yr = (qhdr.Msgdate[6] - '0') * 10 + (qhdr.Msgdate[7] - '0');

        if (yr > 79) {
            yr += 1900;
        } else {
            yr += 2000;
        }

        int mon = (qhdr.Msgdate[0] - '0') * 10 + (qhdr.Msgdate[1] - '0');

        int day = (qhdr.Msgdate[3] - '0') * 10 + (qhdr.Msgdate[4] - '0');

        date.setDate(yr, mon, day);
        QTime time;

        int hour = (qhdr.Msgtime[0] - '0') * 10 + (qhdr.Msgtime[1] - '0');
        int min = (qhdr.Msgtime[3] - '0') * 10 + (qhdr.Msgtime[4] - '0');
        time.setHMS(hour, min, 0);

        QDateTime dt;
        dt.setDate(date);
        dt.setTime(time);

        QString body = rstrip(QString((char *)msg_body));

        if (qwkesupport) {
            bool gotkludge = false;
            while (true) {
                if (body.left(8).toLower() == "subject:") {
                    Subject = "";
                    int i;
                    for (i=8;i<body.length();i++) {
                        if (body.at(i) == '\n') {
                            break;
                        }
                        Subject.append(body.at(i));
                    }
                    body.remove(0, i + 1);
                    Subject = Subject.trimmed();
                    gotkludge = true;
                } else if (body.left(3).toLower() == "to:") {
                    To = "";
                    int i;
                    for (i=3;i<body.length();i++) {
                        if (body.at(i) == '\n') {
                            break;
                        }
                        To.append(body.at(i));
                    }
                    body.remove(0, i + 1);
                    To = To.trimmed();
                    gotkludge = true;
                    } else if (body.left(5).toLower() == "from:") {
                        From = "";
                        int i;
                        for (i=5;i<body.length();i++) {
                            if (body.at(i) == '\n') {
                                break;
                        }
                        From.append(body.at(i));
                    }
                    body.remove(0, i + 1);
                    From = From.trimmed();
                    gotkludge = true;
                } else {
                    if (gotkludge && body.at(0) == '\n') {
                        body.remove(0, 1);
                    }
                    break;
                }
            }
        }

        body.replace(QRegExp("\\x1B\\[[0-9|;|?]*[A-Z|a-z]"), "");

        if (qhdr.Msgstat != 0x56) { // Don't add Synchronet "Voting" messages.
            Message *msg  = new Message(QString((char *)qhdr.Msgnum).left(7).trimmed().toInt(), Subject, From, To, dt, body);

            msg->offset = offset;
            if (qhdr.Msgstat == '-') {
                msg->read = true;
            } else {
                msg->read = false;
            }

            if (msg->to.toLower() == username.toLower()) {
                msg->personal = true;
            } else {
                msg->personal = false;
            }

            int areano = 0xfffff & ((qhdr.Msgareahi << 8) | qhdr.Msgarealo);
            bool found = false;
            for (int i = 0; i< groups->count(); i++) {
                if (groups->at(i)->groupno == areano) {
                    found = true;
                    groups->at(i)->msgs->append(msg);
                    if (msg->personal) {
                        groups->at(i)->personal++;
                    }
                    if (!msg->read) {
                        groups->at(i)->unread++;
                    }
                    break;
                }
            }

            if (!found) {
                messagegroup *mg = new messagegroup(areano);
                mg->msgs->append(msg);
                if (msg->personal) {
                    mg->personal++;
                }
                groups->append(mg);
            }
        }
        free(msg_body);
    }

    fclose(fptr);

    return true;
}
