#include <QtNetwork/QNetworkAccessManager>
#include <QSaveFile>
#include <QStandardPaths>
#include <QSettings>
#include "mainwindow.h"
#include "fetchdialog.h"
#include "ui_fetchdialog.h"

FetchDialog::FetchDialog(QWidget *parent, bool down, QByteArray *data) :
    QDialog(parent),
    ui(new Ui::FetchDialog)
{
    ui->setupUi(this);
    this->mw = (MainWindow *)parent;

    up_or_down = down;

    payload = data;

    QSettings settings("Andrew Pamment", "TitanMail");
    ui->ftp_server->setText(settings.value("ftp_server", "vert.synchro.net").toString());
    ui->ftp_port->setText(settings.value("ftp_port", "21").toString());
    ui->ftp_file->setText(settings.value("ftp_file", "VERT").toString());
    ui->ftp_username->setText(settings.value("ftp_username", "").toString());
    ui->ftp_password->setText(settings.value("ftp_password", "").toString());

    if (down) {
        ui->ftp_file->setText(ui->ftp_file->text() + ".QWK");
    } else {
        ui->ftp_file->setText(ui->ftp_file->text() + ".REP");
    }
}

FetchDialog::~FetchDialog()
{
    delete ui;
}

void FetchDialog::on_buttonBox_accepted()
{
    QUrl url;
    mw->setEnabled(false);
    QSettings settings("Andrew Pamment", "TitanMail");
    settings.setValue("ftp_server", ui->ftp_server->text());
    settings.setValue("ftp_port", ui->ftp_port->text());
    settings.setValue("ftp_username", ui->ftp_username->text());
    settings.setValue("ftp_password", ui->ftp_password->text());

    QString file = ui->ftp_file->text();

    if (up_or_down) {
        if (!file.endsWith(".QWK", Qt::CaseInsensitive)) {
            file.append(".QWK");
        }
    } else {
        if (!file.endsWith(".REP", Qt::CaseInsensitive)) {
            file.append(".REP");
        }
    }




    url.setScheme("ftp");
    url.setHost(ui->ftp_server->text());
    url.setPort(ui->ftp_port->text().toUInt());
    url.setPath(file);
    url.setUserName(ui->ftp_username->text());
    url.setPassword(ui->ftp_password->text());

    file.truncate(file.length() - 4);
    settings.setValue("ftp_file", file);
    QNetworkReply *reply;
    if (up_or_down) {
        reply = this->mw->qnm->get(QNetworkRequest(url));
    } else {
        reply = this->mw->qnm->put(QNetworkRequest(url), *payload);
    }
    qDebug() << (QString)reply->readAll();
}



void FetchDialog::on_buttonBox_rejected()
{

}

