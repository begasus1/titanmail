#include <QSettings>
#include <QFileDialog>
#include <QStandardPaths>
#include "settings.h"
#include "ui_settings.h"

Settings::Settings(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Settings)
{
    ui->setupUi(this);
    this->parent = (MainWindow *)parent;
    ui->unarcEdit->setText(this->parent->unarchive_cmd);
    ui->arcEdit->setText(this->parent->archive_cmd);
    ui->sigEdit->setPlainText(this->parent->signature);
    ui->editorFont->setCurrentFont(QFont(this->parent->editor_font));
    ui->editorFontSize->setValue(this->parent->editor_font_size);
    ui->qwkeCheckbox->setChecked(this->parent->qwkesupport);
    ui->taglineEdit->setText(this->parent->tagline_file);
    ui->extZip->setChecked(!this->parent->intzip);
    ui->intZip->setChecked(this->parent->intzip);
}

Settings::~Settings()
{
    delete ui;
}

void Settings::on_buttonBox_accepted()
{
    parent->unarchive_cmd = ui->unarcEdit->text();
    parent->archive_cmd = ui->arcEdit->text();
    parent->signature = ui->sigEdit->toPlainText();
    parent->editor_font = ui->editorFont->currentFont().family();
    parent->editor_font_size = ui->editorFontSize->value();
    parent->qwkesupport = ui->qwkeCheckbox->isChecked();
    parent->tagline_file = ui->taglineEdit->text();
    if (ui->intZip->isChecked()) {
        parent->intzip = true;
    }
    QSettings st("Andrew Pamment", "TitanMail");
    st.setValue("unarchive_cmd", ui->unarcEdit->text());
    st.setValue("archive_cmd", ui->arcEdit->text());
    st.setValue("signature", ui->sigEdit->toPlainText());
    st.setValue("editor_font", parent->editor_font);
    st.setValue("editor_font_size", parent->editor_font_size);
    st.setValue("qwke_support", ui->qwkeCheckbox->isChecked());
    st.setValue("tagline_file", ui->taglineEdit->text());
    st.setValue("internal_zip", parent->intzip);
    parent->doupdate();
}

void Settings::on_taglineToolBtn_clicked()
{
    QString filename = QFileDialog::getOpenFileName(this, tr("Select a tagline file"), QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation).first(), tr("Text Files (*.txt);;All Files (*.*)"));
    if (!filename.isNull()) {
        ui->taglineEdit->setText(filename);
    }
}
